import { StoreModal } from './../modals/store-modal';
import { ProductModal } from './../modals/product-modal';
import { error } from 'util';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {
  url = 'http://192.168.1.10/Radiant%20Delhi%20Website/RadiantDelhi/api/search.php?q=';
  constructor(private http: Http) {}

  searchStore(searchString: string, onStoreRecieved: (store: StoreModal) => void): void {
    const observable: Observable<StoreModal> = this.http.get(this.url + searchString)
      .map(response => response.json() as StoreModal);
    observable.subscribe({
      next: (store) => {
        onStoreRecieved(store);
      }, error: (error) => {
        alert('error' + error);
      }
    });
  }

}
