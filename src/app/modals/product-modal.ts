export class ProductModal {
  public id: number;
  public name: string;
  public category: string;
  public image: string;
  public about: string;
}
