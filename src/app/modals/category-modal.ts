export class CategoryModal {
  public id: number;
  public name: string;
  public image: string;
  public about: string;
}
