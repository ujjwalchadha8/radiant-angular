import { CategoryModal } from './category-modal';
import { ProductModal } from './product-modal';
export class StoreModal {
  products: ProductModal[];
  categories: CategoryModal[];
}
