import {Global} from './global';
import {NavbarComponent} from './navbar/navbar.component';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {

  title = 'app';
  backgroundIndex = 0;

  backgroundUrls = ['assets/images/background_home.jpg'
    , 'assets/images/background_products.jpg'
    , 'assets/images/background_about.jpg'
    , 'assets/images/background_contact_us.jpg'];

  constructor() {}

  ngOnInit(): void {}

  onNavigation(navNumber: number): void {
    this.backgroundIndex = navNumber;
  }

  getBackgroundUrl(): string {
    return this.backgroundUrls[this.backgroundIndex];
  }
}
