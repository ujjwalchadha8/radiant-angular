import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  LOGOS = ['assets/images/henkel_logo.png', 'assets/images/loctite_logo.png', 'assets/images/molygraph_logo.png'];
  DONUT = '&#x25cf;';

  constructor() {}

  ngOnInit() {
  }

}
