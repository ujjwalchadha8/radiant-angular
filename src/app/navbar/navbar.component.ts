import { StoreModal } from './../modals/store-modal';
import { ProductModal } from '../modals/product-modal';
import {ProductService} from '../services/product.service';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [ProductService]
})
export class NavbarComponent implements OnInit {

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  searchAllowed = false;
  searchString = '';
  storeSearchResult: StoreModal = null;

  constructor(private productService: ProductService) {}

  ngOnInit() {}

  searchProducts(): void {
    this.storeSearchResult = null;
    if (this.searchString === '') { return; }
    this.productService.searchStore(this.searchString, (store: StoreModal): void => {
      if (store.products.length === 0 && store.categories.length === 0) {
        this.storeSearchResult = null;
      }else {
        this.storeSearchResult = store;
      }
    });
  }

  notifyNavigation(navNumber): void {
    this.notify.emit(navNumber);
  }

}
